//////////////////////////////////////////////////////////////////////////////
// FILE   : struts.tol
// PURPOSE: defines structures used in package GrzLinModel
//////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//NrmUnfSclDst: 
//  Possibly truncated normal or uniform scalar distribution
//  Defines an escalar distribution of one of these types:
//    non truncated normal
//    non truncated uniform
/////////////////////////////////////////////////////////////////////////////
Struct @NrmUnfSclDst
{
  Text Name,       // Unique identidier of the parameter
  Real Nu,         // Average for normal case
  Real Sigma       // Sigma for normal case and ? or 1/0 for uniform
};

/////////////////////////////////////////////////////////////////////////////
//PsbTrnNrmUnfSclDst: 
//  Possibly truncated normal or uniform scalar distribution
//  Defines an escalar distribution of one of these types:
//    truncated normal
//    truncated uniform
//    non truncated normal
//    non truncated uniform
/////////////////////////////////////////////////////////////////////////////
Struct @PsbTrnNrmUnfSclDst
{
  Text Name,       // Unique identidier of the parameter
  Real Nu,         // Average for normal case
  Real Sigma,      // Sigma for normal case and ? or 1/0 for uniform
  Real LowerBound, // -1/0 or ? if there is no lower bound
  Real UpperBound  // +1/0 or ? if there is no upper bound
};


/////////////////////////////////////////////////////////////////////////////
//BivTNrm: 
//  Possibly truncated normal or uniform bivariate distribution
//  Defines a bivariate distribution of one of these types:
//    truncated normal
//    truncated uniform
//    non truncated normal
//    non truncated uniform
/////////////////////////////////////////////////////////////////////////////
Struct @BivTNrm
{
  Real sampleLength;
  Real X.mean;
  Real X.sd;
  Real X.lower;
  Real X.upper;
  Real Y.mean;
  Real Y.sd;
  Real Y.lower;
  Real Y.upper;
  Real XY.cor
};



/////////////////////////////////////////////////////////////////////////////
//PsbTrnBetaUnfSclDst: 
//  Possibly truncated beta or uniform scalar distribution
//  Defines an escalar distribution of one of these types:
//    truncated beta
//    truncated uniform
//    non truncated beta
//    non truncated uniform
/////////////////////////////////////////////////////////////////////////////
Struct @PsbTrnBetaUnfSclDst
{
  Real k,          // Unique identidier of the unknown output row
  Real a,          // First parameter of Beta (0 for uniform)
  Real b,          // Second parameter of Beta (0 for uniform)
  Real LowerBound, // 0, -1/0 or ? if there is no lower bound
  Real UpperBound  // 1, +1/0 or ? if there is no upper bound
};


/////////////////////////////////////////////////////////////////////////////
//PsbTrnNrmUnfCombDst: 
//  Possibly truncated normal or uniform scalar distribution over a linear 
//  combination of variables
//  Defines an escalar distribution of one of these types:
//    truncated normal
//    truncated uniform
//    non truncated normal
//    non truncated uniform
/////////////////////////////////////////////////////////////////////////////
Struct @PsbTrnNrmUnfVecDst
{
  Text Name, 
  Set  Parameters, 
  Set  Coefficients, 
  Real Nu, 
  Real Sigma,
  Real LowerBound, // 0, -1/0 or ? if there is no lower bound
  Real UpperBound  // 1, +1/0 or ? if there is no upper bound
};

/////////////////////////////////////////////////////////////////////////////
Struct @OrderRelation
/////////////////////////////////////////////////////////////////////////////
{
  Text Name, 
  Text LowerParam, 
  Text UpperParam
};

/////////////////////////////////////////////////////////////////////////////
Struct @EqualParameters
/////////////////////////////////////////////////////////////////////////////
{
  Text EqName, 
  Set  OriginalNames
};

/////////////////////////////////////////////////////////////////////////////
Struct @LinearEqualities
//
// H*alpha=h
/////////////////////////////////////////////////////////////////////////////
{
  Text EqName, 
  Set  OriginalNames,
  VMatrix H,
  VMatrix h
};



  
  
